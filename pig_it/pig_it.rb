# frozen_string_literal: true

def pig_it(text)
  text.gsub(/(\w)(\w*)/, '\2\1ay')
end
