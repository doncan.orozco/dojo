# frozen_string_literal: true

class RomanNumerals
  ROMAN_EQUIVALENCES = {
    M: 1000,
    CM: 900,
    D: 500,
    CD: 400,
    C: 100,
    XC: 90,
    L: 50,
    XL: 40,
    X: 10,
    IX: 9,
    V: 5,
    IV: 4,
    I: 1
  }.freeze

  def self.to_roman(number)
    roman = []
    ROMAN_EQUIVALENCES.each do |roman_symbol, equivalence_value|
      while number >= equivalence_value
        number -= equivalence_value
        roman << roman_symbol.to_s
      end
    end
    roman.join
  end

  def self.from_roman(roman_number)
    roman_numbers = roman_number.scan(/M|CM|D|CD|C|XC|L|XL|X|IX|V|IV|I/)
    roman_numbers.reduce(0) { |sum, roman_symbol| sum + ROMAN_EQUIVALENCES[roman_symbol.to_sym] }
  end
end
