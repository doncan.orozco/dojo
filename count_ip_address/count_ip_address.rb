# frozen_string_literal: true

def ipsBetween(start, ending)
  ips_value(ending) - ips_value(start)
end

def ips_value(ips)
  ips.gsub(/(\d+)(\.?)/) { format('%08b', Regexp.last_match(1)) }.to_i(2)
end
