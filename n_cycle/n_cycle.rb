# frozen_string_literal: true

def cycle(n)
  decimals = decimals_generator(n)

  decimal_serie = []
  (1..n).each do |cycle|
    decimal_serie.concat [decimals.next, decimals.next]
    return cycle if decimal_serie[0, cycle] == decimal_serie[cycle, cycle] && !decimal_serie[0, cycle].all?(&:zero?)
  end

  -1
end

def decimals_generator(denominator)
  Enumerator.new do |yielder, modulus: 1|
    loop do
      quotient, modulus = (10 * modulus).divmod(denominator)
      yielder << quotient
    end
  end
end
