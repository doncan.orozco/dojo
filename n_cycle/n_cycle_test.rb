# frozen_string_literal: true

require_relative 'n_cycle'
require 'test/unit'

# Test cases for n_cycle kata
class NCycleTest < Test::Unit::TestCase
  def test_cases
    assert_equal cycle(5), -1
    assert_equal cycle(13), 6
    assert_equal cycle(21), 6
    assert_equal cycle(27), 3
    assert_equal cycle(33), 2
    assert_equal cycle(37), 3
    assert_equal cycle(94), -1
  end

  def test_number_with_a_really_big_cycle
    assert_equal cycle(29), 28
  end
end
