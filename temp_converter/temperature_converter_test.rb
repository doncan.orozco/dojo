# frozen_string_literal: true

require_relative 'temperature_converter'
require 'test/unit'

# Test cases for rgb_to_hex_conversion kata
class TemperatureconverterTest < Test::Unit::TestCase
  def convert_temp(grades, input_scale:, output_scale: 'celsius')
    TemperatureConverter.new(input_scale, output_scale).convert_temp(grades)
  end

  def test_cases
    assert_equal 373.15, convert_temp(100, input_scale: 'celsius', output_scale: 'kelvin')
    assert_equal 212.0, convert_temp(100, input_scale: 'celsius', output_scale: 'fahrenheit')
    assert_equal(-173.14999999999998, convert_temp(100, input_scale: 'kelvin', output_scale: 'celsius'))
    assert_equal(-279.66999999999996, convert_temp(100, input_scale: 'kelvin', output_scale: 'fahrenheit'))
    assert_equal 37.77777777777778, convert_temp(100, input_scale: 'fahrenheit', output_scale: 'celsius')
    assert_equal 310.92777777777775, convert_temp(100, input_scale: 'fahrenheit', output_scale: 'kelvin')
  end
end
