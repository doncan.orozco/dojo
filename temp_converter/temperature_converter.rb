# frozen_string_literal: true

class TemperatureConverter
  attr_reader :input_scale, :output_scale

  def initialize(input_scale, output_scale)
    @input_scale = input_scale
    @output_scale = output_scale
  end

  def convert_temp(grades)
    converter = "#{input_scale}_to_#{output_scale}"
    raise StandardError, 'Invalid scales' unless respond_to? converter, true

    send(converter, grades)
  end

  private

  def celsius_to_kelvin(grades)
    grades + 273.15
  end

  def celsius_to_fahrenheit(grades)
    (grades * 9.0 / 5) + 32
  end

  def kelvin_to_celsius(grades)
    grades - 273.15
  end

  def kelvin_to_fahrenheit(grades)
    celsius = kelvin_to_celsius(grades)
    celsius_to_fahrenheit(celsius)
  end

  def fahrenheit_to_celsius(grades)
    (grades - 32) * 5.0 / 9
  end

  def fahrenheit_to_kelvin(grades)
    celsius = fahrenheit_to_celsius(grades)
    celsius_to_kelvin(celsius)
  end
end
