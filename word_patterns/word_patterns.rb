# frozen_string_literal: true

def word_pattern(pattern, string)
  pattern_pairs = pattern.chars.zip(string.split)
  pattern_translation = pattern_pairs.to_h

  return false unless pattern_translation.values.uniq == pattern_translation.values

  pattern_pairs.all? { |code, word| pattern_translation[code] == word }
end
