# frozen_string_literal: true

def create_phone_number(numbers)
  "(#{numbers[0, 3].join}) #{numbers[3, 3].join}-#{numbers[6, 4].join}"
end
