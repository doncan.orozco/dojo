# frozen_string_literal: true

SECRET = ['qrygn', 'zrrg ng pubpbyngr pbeare', 'gra zra', 'gjb onpxhc grnzf', 'zvqavtug rkgenpgvba'].freeze
SECRET2 = ['Jul qvq gur puvpxra pebff gur ebnq?'].freeze

ALPHABET_UPPERCASE = ('A'..'Z').to_a
ALPHABET_LOWERCASE = ('a'..'z').to_a

new_array = SECRET.map do |string|
  string.each_char.map do |char|
    ALPHABET_UPPERCASE.include?(char) ? ALPHABET_UPPERCASE[ALPHABET_UPPERCASE.index(char) - 13] : char
  end.join
end

new_array = new_array.map do |string|
  string.each_char.map do |char|
    ALPHABET_LOWERCASE.include?(char) ? ALPHABET_LOWERCASE[ALPHABET_LOWERCASE.index(char) - 13] : char
  end.join
end

new_array
